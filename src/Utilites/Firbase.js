import { initializeApp } from "firebase/app";
import {getAuth, GoogleAuthProvider} from 'firebase/auth'
import { getFirestore } from 'firebase/firestore'

const firebaseConfig = {
  apiKey: "AIzaSyAjOOUdakxQKkZ7nT0SHUxuR2hvMCE35Nc",
  authDomain: "fir-test-project-80404.firebaseapp.com",
  projectId: "fir-test-project-80404",
  storageBucket: "fir-test-project-80404.appspot.com",
  messagingSenderId: "401460684889",
  appId: "1:401460684889:web:b003781a8ec2d36792e948"
};

export const app = initializeApp(firebaseConfig);
export const auth = getAuth();
export const db = getFirestore(app)
export const provider = new GoogleAuthProvider()
