import Form from "./Page/Form";
import LogIn from "./Auth/LogIn";
import {Route, Routes} from "react-router-dom";
import SingIn from "./Auth/SingIn";


function App() {
  return (
    <div>
        <Routes>
          <Route path='/Form' element={<Form/>}/>
          <Route index element={<LogIn/>}/>
          <Route path='/SingIn' element={<SingIn/>}/>
        </Routes>
    </div>
  );
}

export default App;
