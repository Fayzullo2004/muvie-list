import React, {useEffect, useState} from 'react';
import {useNavigate} from "react-router-dom";
import SinginWithGoogle from "../Auth/SinginWithGoogle";

function Form() {
  const [todos, setTodos] = useState(() => {
    const savedTodos = localStorage.getItem("todos");
    if (savedTodos) {
      return JSON.parse(savedTodos);
    } else {
      return [];
    }
  });

  const [todo, setTodo] = useState("");
  const [isEditing, setIsEditing] = useState(false);
  const [currentTodo, setCurrentTodo] = useState({});
  const Navigate = useNavigate()

  useEffect(() => {
    localStorage.setItem("todos", JSON.stringify(todos));
  }, [todos]);

  const  handleInputChange = (e) => {
    setTodo(e.target.value);
  }

  const handleEditInputChange = (e) => {
    setCurrentTodo({ ...currentTodo, text: e.target.value });
    console.log(currentTodo);
  }

  const handleFormSubmit = (e) => {
    e.preventDefault();

    if (todo !== "") {
      setTodos([
        ...todos,
        {
          id: todos.length + 1,
          text: todo.trim()
        }
      ]);
    }

    setTodo("");
  }

  const handleEditFormSubmit = (e) => {
    e.preventDefault();

    handleUpdateTodo(currentTodo.id, currentTodo);
  }

  const handleDeleteClick = (id) => {
    const removeItem = todos.filter((todo) => {
      return todo.id !== id;
    });
    setTodos(removeItem);
  }

  const handleUpdateTodo = (id, updatedTodo) => {

    const updatedItem = todos.map((todo) => {
      return todo.id === id ? updatedTodo : todo;
    });

    setIsEditing(false);

    setTodos(updatedItem);
  }


  const handleEditClick = (todo) => {

    setIsEditing(true);

    setCurrentTodo({ ...todo });
  }

  const LogOut = () => {
    Navigate('/')
  }

  return (
    <div className="App">
      <h1
        className='bg-red-600 w-36 text-center text-white rounded-xl pb-2 cursor-pointer'
        onClick={LogOut}
      >
        Log Out
      </h1>

      {isEditing ? (

        <form onSubmit={handleEditFormSubmit}>

              <h2 className='text-center'>Edit Todo</h2>

              <label className='text-center'>Edit todo: </label>
          <div>
            <div className='flex justify-center'>
              <input
                className="bg-blue-100"
                name="editTodo"
                type="text"
                placeholder="Edit todo"
                value={currentTodo.text}
                onChange={handleEditInputChange}
              />

              <button type="submit" className="ml-7">Update</button>

              <button onClick={() => setIsEditing(false)}>Cancel</button>
            </div>
            </div>

        </form>
      ) : (
        <form onSubmit={handleFormSubmit}>

          <h2 className='text-center'> Todo : {todos.length}  </h2>
          <div className='flex justify-center'>
            <div>
              <label >Add todo: </label>

              <input
                className="bg-blue-100"
                name="todo"
                type="text"
                placeholder="Create a new todo"
                value={todo}
                onChange={handleInputChange}
              />
              <button type="submit"  className="ml-7">Add</button>
            </div>
          </div>
        </form>
      )}

      <ul className="todo-list text-center w-1/2">
        {todos.map((todo) => (
          <li key={todo.id} className='flex justify-between w-1/3'>
            {todo.text}
              <div>
                <button onClick={() => handleEditClick(todo)} className="ml-7">Edit</button>
                <button onClick={() => handleDeleteClick(todo.id)} className="ml-7">Delete</button>
              </div>
          </li>
        ))}
      </ul>
    </div>
  );
}

export default Form;