import React, {useEffect, useState} from 'react';
import { provider , auth } from "../Utilites/Firbase";
import { signInWithPopup } from "firebase/auth";
import {useNavigate} from "react-router-dom";

function SinginWithGoogle() {
  const [ value , setValue ] = useState('')
  const Navigate  = useNavigate()

  const Google = async () => {
   await signInWithPopup(auth, provider).then((data) => {
     setValue(data.user.email)
      localStorage.setItem("email" , data.user.email)
      Navigate('/Form')
    })
  }

  useEffect(() => {
    setValue(localStorage.getItem('email'))
  },[])

  return (
    <button
      className='bg-red-500 hover:bg-red-600 text-white w-1/2 py-3 rounded-xl mt-4 text-xl font-bold flex justify-center'
      onClick={Google}
    >
      Google
    </button>
  );
}

export default SinginWithGoogle;
