import { useState } from 'react';
import { signInWithEmailAndPassword } from "firebase/auth";
import { auth } from "../Utilites/Firbase";
import {Link, useNavigate} from "react-router-dom";
import SinginWithGoogle from "./SinginWithGoogle";

function LogIn() {

  const [email , setEmail] = useState('')
  const [password , setPassword] = useState('')
  const Navigation = useNavigate()


  const Login = async () => {
    await signInWithEmailAndPassword(auth, email, password)
      .then((userCredential) => {
        const user = userCredential.user;
        Navigation("/Form")
      })
      .catch((error) => {
        const errorCode = error.code;
        console.log(errorCode)
        const errorMessage = error.message;
        alert(`${email} Bunday Akkaunt Yo'q`)
      });
  }


  return (
    <>
      <div className=' flex justify-center'>
        <form
          className="rounded px-8 pt-6 pb-8 mb-4 w-1/2"
          onSubmit={Login}
        >
          <div className="mb-4">
            <label className="block text-gray-700 text-sm font-bold mb-2">
              Username
            </label>
            <input
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              id="username"
              type="text"
              placeholder="Email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </div>
          <div className="mb-6">
            <label className="block text-gray-700 text-sm font-bold mb-2">
              Password
            </label>
            <input
              className="shadow appearance-none border border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
              id="password"
              type="password"
              placeholder="******************"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </div>
          <div className="flex items-center justify-between">
            <button
              className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
              type="button"
              onClick={Login}
            >
              LOG IN
            </button>
            <Link
              className="inline-block align-baseline font-bold text-sm text-blue-500 hover:text-blue-800"
              to="/SingIn"
            >
              Create account
            </Link>
          </div>
        </form>
      </div>
      <SinginWithGoogle/>
    </>
  );
}

export default LogIn;