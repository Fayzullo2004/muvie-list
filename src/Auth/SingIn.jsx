import React, {useState} from 'react';
import { createUserWithEmailAndPassword } from "firebase/auth";
import { auth } from "../Utilites/Firbase";
import {useNavigate} from "react-router-dom";
import SinginWithGoogle from "./SinginWithGoogle";

function SingIn() {

  const [email , setEmail] = useState('')
  const [password , setPassword] = useState('')
  const Navigate = useNavigate()

  const  singIn = () => {
    createUserWithEmailAndPassword(auth, email, password)
      .then((userCredential) => {
        const user = userCredential.user;
        console.log(user)
        Navigate("/Form")
        console.log("hello world !")
      })
      .catch((error) => {
        const errorCode = error.code;
        const errorMessage = error.message;
        alert(`${email} Bunday Akkaunt Avval Ro'yxatdan O'tgan`)
      });
  }


  return (
    <>
      <div className='flex justify-center'>
      <form className="rounded px-8 pt-6 pb-8 mb-4 w-1/2">
        <div className="mb-4">
          <label className="block text-gray-700 text-sm font-bold mb-2">
            Username
          </label>
          <input
            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            id="username"
            type="text"
            placeholder="Email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </div>
        <div className="mb-6">
          <label className="block text-gray-700 text-sm font-bold mb-2">
            Password
          </label>
          <input
            className="shadow appearance-none border border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
            id="password"
            type="password"
            placeholder="******************"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </div>
        <div className="flex items-center justify-between">
          <button
            className="w-full bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
            type="button"
            onClick={singIn}
          >
              SING IN
          </button>
        </div>
      </form>
      </div>
      <SinginWithGoogle/>
    </>
  );
}

export default SingIn;